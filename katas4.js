/* eslint-disable  */
const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ['Mordor', 'Gondor', 'Rohan', 'Beleriand', 'Mirkwood', 'Dead Marshes', 'Rhun', 'Harad'];
const bestThing = 'The best thing about a boolean is even if you are wrong you are only off by a bit';




const kata1 = () => {
    create(JSON.stringify(gotCitiesCSV.split(',')));
};
kata1();

const kata2 = () => {
    create(JSON.stringify(bestThing.split(' ')));
};
kata2();
const kata3 = () => {
    create(JSON.stringify(gotCitiesCSV.replace(/,/gi, ': ')));
};
kata3();


const kata4 = () => {
    create(JSON.stringify(lotrCitiesArray.join(', ')));
};
kata4();

const kata5 = () => {
    let temp = lotrCitiesArray.slice(0, 5);
    create(JSON.stringify(temp));
};
kata5();
const kata6 = () => {
    let temp = lotrCitiesArray.slice(-5);
    create(JSON.stringify(temp));
};
kata6();
const kata7 = () => {
    let temp = lotrCitiesArray.slice(2, 5);
    create(temp);
};
kata7();

function kata8() {
    (lotrCitiesArray.splice(-6, 1));
    create(lotrCitiesArray)
}
kata8();
const kata9 = () => {
    (lotrCitiesArray.splice(5, 2));
    create(lotrCitiesArray)
}
kata9();
const kata10 = () => {
    lotrCitiesArray.splice(2, 0, 'Rohan');
    create(lotrCitiesArray)
};
kata10();
const kata11 = () => {
    lotrCitiesArray.splice(5, 1, 'Deadest Marshes');
    create(lotrCitiesArray);
};
kata11();
const kata12 = () => {
    create(JSON.stringify(bestThing.slice(0, 14)));
};
kata12();
const kata13 = () => {
    create(JSON.stringify(bestThing.slice(-12)));
};
kata13();
const kata14 = () => {
    create(JSON.stringify(bestThing.slice(23, 38)));
};
kata14();
const kata15 = () => {
    create(JSON.stringify(bestThing.substring(69)));
};
kata15();
const kata16 = () => {
    create(JSON.stringify(bestThing.substring(23, 38)));
};
kata16();
const kata17 = () => {
    create(JSON.stringify(bestThing.indexOf('only')));
};
kata17();
let word = [];
const kata18 = function () {
    let tempString = bestThing;
    var n = tempString.split(" ");
    let lastWords = n[n.length - 1];
    word.push(lastWords);
    let lastIndex = bestThing.lastIndexOf(lastWords);
    create('The last word is ' + word + ' and index is ' + lastIndex + '.');
};
kata18();

function checkCities(city) {
    if (
        city.includes("aa") ||
        city.includes("ee") ||
        city.includes("ii") ||
        city.includes("oo") ||
        city.includes("uu")
    ) {
        return city;
    }
}
const kata19 = () => {
    create(JSON.stringify(gotCitiesCSV.split(",").filter(checkCities)))
}
kata19();
const kata20 = function () {
    function check(city) {
        if (city.slice(-2) === "or") {
            return city;
        }
    }
    create(lotrCitiesArray.filter(check));
}
kata20();
const kata21 = function () {
    function checkFirstLetter(word) {
        if (word.charAt(0) === "b") {
            return word;
        }
    }
    create(bestThing.split(" ").filter(checkFirstLetter));
}
kata21();
const cityYn = function (city) {
    if (lotrCitiesArray.includes(city)) {
        return 'Yes'
    } else {
        return 'No'
    }
}
const kata22 = () => {
    create(cityYn('Mirkwood'))
};
kata22();
const kata23 = () => {
    create(cityYn('Hollywood'));
}
kata23();
const kata24 = () => {
    let newCity = lotrCitiesArray.indexOf('Mirkwood');
    create('The index of Mirkwood: ' + newCity);
}
kata24();
const kata25 = () => {
    create(lotrCitiesArray[0]);
};
kata25();
const kata26 = () => {
    create(lotrCitiesArray.reverse());
};
kata26();
const kata27 = () => {
    create(lotrCitiesArray.sort());
}
kata27();
const kata28 = () => {
    lotrCitiesArray.sort(function (a, b) {
        return a.length - b.length; //ASC, For Descending order use: b - a
    });
    create(lotrCitiesArray);
}
kata28();
const kata29 = () => {
    lotrCitiesArray.pop();
    create(lotrCitiesArray);
};
kata29();
const kata30 = () => {
    lotrCitiesArray.push('Harad');
    create(lotrCitiesArray);
};
kata30();
const kata31 = () => {
    lotrCitiesArray.shift();
    create(lotrCitiesArray);
};
kata31();
const kata32 = () => {
    lotrCitiesArray.unshift('Mordor');
    create(lotrCitiesArray)
}
kata32();

function create(kata) {
    var createDiv = document.createElement("div");
    createDiv.className = 'displayDiv'
    var node = document.createTextNode(kata);
    createDiv.appendChild(node);

    var element = document.getElementById("container");
    element.appendChild(createDiv);
}